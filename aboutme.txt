Software development was my initial career path, and I'm glad I discovered it. In college, I originally studied Software Development. After completed my 2 year course, I accepted a job as a customer/technical support representative. And now I've been working in the bpo industry for almost 4 yrs now. While I was good at my job and worked hard, the position didn't excite me, so I searched for something more fulfilling.

After sharing my lack of job satisfaction with 
my cousin, he told me about his career as a software developer. It was something I had never considered, but his description of the field intrigued me. I learned more about the subject via online resources and began to teach myself basic concepts. However, I felt that what I knew was not enough, that's why I tried to apply to zuitt and I was very lucky and was chosen to be included in the bootcamp. I'm very excited to work as a professional software/web developer and use the versatile skills that this career demands.

